var dom = require('./domain.js');
var	User = dom.User,
	Problem = dom.Problem,
	Contest = dom.Contest,
	Group = dom.Group,
	Attempt = dom.Attempt;
var fs = require('fs-extra');
var h = require('./helpers.js');
var path = require('path');

/** provides fs access */
class UserData {
	/** gets all available user objects */
	static getAllUsersSync() {
		var files = [];
		try {
			files = fs.readdirSync(global.config.default_user_path);
			//TODO filter
		} catch (e) { h.err("[getAllUsersSync] error reading dir: " + e.message); }
		return files.map((p) => {
			try {
				var u = UserData.getUser(p);
				return u;
			} catch (e2) {
				 h.err("[getAllUsersSync] error constructing user: " + p +
				 			". Reason: " + e2.message);
			}
		}).filter((p)=>{return !!p;});
	}

	static filterDirs(loc, files) {
		var r = [];
		if (!files || files.length == 0) return r;
		for (var x in files) {
			var fname = path.join(loc, files[x]);
			var stat = fs.statSync(fname);
			if (stat.isDirectory()) {
				r.push(files[x]);
			}
		}
		return r;
	}

	static filterFiles(loc, files) {
		var r = [];
		for (var x in files) {
			var stat = fs.statSync(path.join(loc, files[x]));
			if (stat.isFile()) {
				r.push(files[x]);
			}
		}
		return r;
	}

	/** gets all contest objects */
	static getAllContestsSync() {
		var files = [];
		try {
			var loc = global.config.default_contest_path;
			files = fs.readdirSync(loc);
			files = UserData.filterFiles(loc, files);
		} catch (e) { h.err("[getAllContestsSync] " + e.message); }
		return files
						.map((p) => UserData.getContest(p))
						.filter((p)=>{return !!p;});
	}

	/** gets all problem names */
	static getAllProblemNamesSync() {
		var files = [];
		try {
			var loc = global.config.default_testdata_path;
			files = fs.readdirSync(loc);
			files = UserData.filterDirs(loc, files);
		} catch (e) { h.err("[getAllProblemNamesSync] " + e.message); }
		return files;
	}

	/** gets all problem objects */
	static getAllProblemsSync() {
		var files = [];
		try {
			var loc = global.config.default_testdata_path;
			files = fs.readdirSync(loc);
			files = UserData.filterDirs(loc, files);
		} catch (e) { h.err("[getAllProblemsSync] " + e.message); }
		return files.map((p) => UserData.getProblem(p));
	}

	/** gets all group names */
	static getAllGroupNamesSync() {
		var files = [];
		try {
			var loc = global.config.default_group_path;
			files = fs.readdirSync(loc);
			files = UserData.filterFiles(loc, files);
		} catch(e) { h.err("[getAllGroupNamesSync] " + e.message); }
		return files;
	}

	/** gets all group objects */
	static getAllGroupsSync() {
		var files = [];
		try {
			var loc = global.config.default_group_path;
			files = fs.readdirSync(loc);
			files = UserData.filterFiles(loc, files);
		} catch(e) { h.err("[getAllGroupsSync] " + e.message); }
		return files.map((p) => UserData.getGroup(p));
	}

	/** gets all problems by IDS */
	static getAllProblemsByIdsSync(problemIds) {
		if (!problemIds || problemIds.length == 0) return [];
		var files = [];
		try {
			var loc = global.config.default_testdata_path;
			files = fs.readdirSync(loc) || [];
			files = UserData.filterDirs(loc, files);
		} catch (e) {
			h.err("[getAllProblemsByIdsSync] " + e.message);
		}

		return files
				.filter((p) => { return problemIds.indexOf(p) > -1; })
				.map((p) => UserData.getProblem(p))
				.filter((p)=>{return !!p;});
	}

	static getContestUsersSync(contest) {
		var result = [];
		for (var g in contest.groups) {
			var users = UserData.getGroupUsersByIdSync(contest.groups[g]);
			for (var u in users) result.push(users[u]);
		}
		return result;
	}

	static getGroupUsersByIdSync(group) {
		var users = [];
		var gr = UserData.getGroup(group);
		if (gr.users) {
			for (var u in gr.users) {
				try {
					var user = UserData.getUser(gr.users[u]);
					if (user) users.push(user);
				} catch(e) {
					h.err("[getGroupUsersByIdSync] Error while reading users from group " +
						gr.id + ". Non-existing user " +
						gr.users[u] + ".");
				}
			}
		}
		return users;
	}

	//TODO: add cache
	static getUser(login) {
		return new dom.User(login);
	}

	static userExists(login) {
		return dom.User.exists(login);
	}

	static saveUser(userObj) {
		userObj.persist();
	}

	static getContest(cid) {
		try {
			var c = new Contest(cid);
			return c;
		} catch (e) {
			h.err("[dal:getContest(cid)] " + e.message);
			return null;
		}
	}

	static getGroup(gid) {
		return new Group(gid);
	}

	static getProblem(pid) {
		try {
			return new Problem(pid);
		} catch (e) {
			h.err("[dal:getProblem(pid)] " + e.message);
			return null;
		}
	}

	static getAttempt(contest, problem, user, attempt) {
		var a = new Attempt(contest, problem, user, attempt);
		a = h.ensureTextGoodEnoughForDisplay(a);
		return a;
	}
}

module.exports = { UserData }
