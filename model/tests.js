var helpers =	require('./helpers.js');

// test object that just matches line-by-line
class MatchTest {
	constructor(file1, file2, isStrict) { 
        this.f1 = file1; 
        this.f2 = file2; 
        this.isStrict = isStrict; 
    }
    
	test() {
        var match = helpers.easymatch(this.f1, this.f2);
        if (this.isStrict) {
            match.reason = "[classified :)]";
        }
        return match; 
    }
}

class LineSetMatchTest {
	constructor(file1, file2, isStrict) { 
        this.f1 = file1; 
        this.f2 = file2; 
        this.isStrict = isStrict; 
    }

    test() {
        var match = helpers.lineseqmatch(this.f1, this.f2);
        if (this.isStrict) {
            match.reason = "[classified :)]";
        }
        return match; 
    }
}

class SeqLengthMatchTest {
	constructor(file1, file2, isStrict) { 
        this.f1 = file1; 
        this.f2 = file2; 
        this.isStrict = isStrict; 
    }

	test() {
        var match = helpers.sequence_size_match(this.f1, this.f2);
        if (this.isStrict) {
            match.reason = "[classified :)]";
        }
        return match;
    }
}

class SetCountMatchTest {
	constructor(file1, file2, isStrict) { 
        this.f1 = file1; 
        this.f2 = file2; 
        this.isStrict = isStrict; 
    }

	test() {
        var match = helpers.seq_one_of_sizes_match(this.f1, this.f2);
        if (this.isStrict) {
            match.reason = "[classified :)]";
        }
        return match;
    }
}

module.exports = {
	MatchTest, LineSetMatchTest, SeqLengthMatchTest, SetCountMatchTest
}
