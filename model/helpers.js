var fs = require('fs-extra');
var path = require('path');
global.config = global.config || require('../config.json');

var ACCESSLOG = 'codetest-access.log';
var LOG = 'codetest-event.log';

const TEXT_LIMIT_K = 20;
var NL = global.config.NEW_LINE;

function general_match(file1base, file2compared, match_lambda) {
	var result_correct = "", result_output = "";
	var result = { code: "OK", reason: "" };
	try {
		var x = fs.readFileSync(file1base, 'utf-8');
		result_correct = x.trim().split(global.config.NEW_LINE);

		//XXX: remove if we want to avoid cheating
		result.reason = x;

		result_output = fs
					.readFileSync(file2compared, 'utf-8')
                    .trim()
					.split(global.config.NEW_LINE);
	} catch (e) {
		result.code = 'FERROR';
		result.reason = e.message;
		return result;
	};
	if (result_correct.length != result_output.length) {
		result.code = 'STRCOUNT';
		var exp = result_correct.length;
		var res = result_output.length;
		result.reason = 'String counts don\'t match: expected ' + exp + ', got ' + res;
		return result;
	} else {
		for (var i = 0; i < result_correct.length; i++) {
			var tt = match_lambda(result_correct[i], result_output[i], i);
			if (tt != null && tt != 'undefined') {
				return tt;
			}
		}
	}
	return result;
}

/** simple algorithm for matching 2 files line-by-line 
  * allow trailing whitespaces
  */
function easymatch(file1base, file2compared) {
	var gm_result = general_match(file1base, file2compared, (golden, res, line) => {
			var exp = golden.trim();
			var rcv = res.trim();
			if (exp != rcv) {
				var r = {};
				r.code = 'STRMATCH';
				r.reason = 'Strings don\'t match: line ' + (line + 1) + NL;
				r.reason += 'Expected: ' + exp + ' ' + NL;
				r.reason += 'Recieved: ' + rcv;
				return r;
			} else {
				return null;
			}
		});
		return gm_result;
}

/** simple algorithm for matching 2 files and comparing sequence length */
function sequence_size_match(file1base, file2compared) {
	var gm_result = general_match(file1base, file2compared, (golden, res, line) => {
			var collection1 = golden
								.split(/\W/).filter((a) => {return !!a;} );
			var collection2 = res
								.split(/\W/).filter((a) => {return !!a;} );
			if (collection1.length != collection2.length) {
				var r = {};
				r.code = 'SEQLENMATCH';
				r.reason = 'Sequence length doesn\'t match: line ' + (line + 1) + NL;
				r.reason += 'Recieved: ' + collection2.length;
				return r;
			} else {
			  return null;
			}
		} );
		return gm_result;
}

/** simple algorithm for matching 2 files and comparing set size */
function set_size_match(file1base, file2compared) {
	var gm_result = general_match(file1base, file2compared, (golden, res, line) => {
			var set1 = golden
										.split(/\W/).filter((a) => {return !!a;})
									  .filter((val, ind, arr) => arr.indexOf(val) === ind);
			var set2 = res
										.split(/\W/).filter((a) => {return !!a;})
										.filter((val, ind, arr) => arr.indexOf(val) === ind);
			if (set1.length != set2.length) {
				var r = {};
				r.code = 'SETSIZEMATCH';
				r.reason = 'Set size doesn\'t match: line ' + (line + 1) + " " + NL;
				r.reason += 'Recieved: ' + set2.length;
				return r;
			} else {
				return null;
			}
		});
	return gm_result;
}

/** simple algorithm for matching if seq size is one of given */
function seq_one_of_sizes_match(file1base, file2compared) {
	var gm_result = general_match(file1base, file2compared, (golden, res, line) => {
			var setCounts = golden
										.split(/\W/).filter((a) => {return !!a;})
									  .map((val) => parseInt(val));

			var collection = res
										.split(/\W/).filter((a) => {return !!a;} );

			// if collection size is NOT one of given
			if (setCounts.indexOf(collection.length) < 0) {
				var r = {};
				r.code = 'SEQLENMATCH';
				r.reason = 'Sequence length doesn\'t match: line ' + (line + 1) + " " + NL;
				r.reason += 'Recieved: ' + collection.length;
				return r;
			} else {
				return null;
			}
		});
	return gm_result;
}

/** simple algorithm for matching 2 files line-by-line,
 * each line should have same sequence of elements
 */
function lineseqmatch(file1base, file2compared) {
	var gm_result = general_match(file1base, file2compared, (golden, res, line) => {
			var r = {};
			// split by whitespaces and return only values sorted
			var seq1 = golden
											.split(/\W/)
											.filter((a) => {return !!a;})
											.sort();
			var seq2 = res
											.split(/\W/)
											.filter((a) => {return !!a;})
											.sort();
			if (seq1.length != seq2.length) {
				r =  {
					code: 'SEQLENCOUNT',
					reason: 'Item count doesn\'t match in line ' + (line+1) + NL
				};
				r.reason += 'Expected: ' + seq1.length + ' ' + NL;
				r.reason += 'Recieved: ' + seq2.length;
				return r;
			} else {
				for (var j = 0; j < seq1.length; j++) {
					if (seq1[j] != seq2[j]) {
						r = {
								code   : 'SEQMATCH',
								reason : 'Sets in line ' + (line+1) + ' don\'t match'
						};
						return r;
					}
				}
			}
			return null;
		} );
	return gm_result;
}

/** support of monade calls
 *	@param {array} queue of objects that will be passed to functions
 *	@param {function} function that will be executed over the data
 *	@param {object|array} resultObject object or array that will accumulate changes/results. Available in closure.
 *	@callback object
 **/
function monade(queue, func, resultObject, callback) {
	if (typeof resultObject == 'function') {
		resultObject = {};
		callback = resultObject;
	}

	var lemonade = (queue, func, callback) => {
		if (!queue.length) {
			callback(resultObject);
		} else {
			func(queue.pop(), resultObject, () => { lemonade(queue, func, callback); } );
		}
	}
	lemonade(queue.reverse(), func, callback);
}

function chmod777(obj) {
	try {
		var stats = fs.statSync(obj);
		if (stats.isFile()) {
			fs.chmodSync(obj, '777');
		} else if (stats.isDirectory()) {
			fs.chmodSync(obj, '777');
			var files = fs.readdirSync(obj);
			for (var i in files) {
				fs.chmodSync(path.join(obj, files[i]), '777');
			}
		}
	} catch (e) {
		err(e);
	}
}

var LEVELS = {
	LOG_OFF			: 0,
	LOG_ERROR_ONLY 	: 1,
	LOG_WARN 		: 2,
	LOG_VERBOSE_ALL : 3,
	LOG_VERBOSE_INCLUDE_DBG : 4
};

function write(file, type, text) {
	try {
		if (typeof(text) == 'object') text = JSON.stringify(text);
		fs.appendFileSync(file, '[' + (new Date()).toUTCString() + '][' + type + '] ' + text + NL);
	} catch (e) {
			console.log(e);
	}
}

// logs information messages for VERBOSE_ALL and DEBUG modes
function log(text) {
	if (global.config.loglevel == LEVELS.LOG_VERBOSE_ALL ||
			global.config.loglevel == LEVELS.LOG_VERBOSE_INCLUDE_DBG)
				write(LOG, "INFO", text);
}

// logs error information: for ERROR, ALL, DEBUG modes
function err(text) {
	if (global.config.loglevel == LEVELS.LOG_ERROR_ONLY ||
			global.config.loglevel == LEVELS.LOG_WARN ||
			global.config.loglevel == LEVELS.LOG_VERBOSE_ALL ||
			global.config.loglevel == LEVELS.LOG_VERBOSE_INCLUDE_DBG) {
			if (typeof(text) == 'object') text = JSON.stringify(text);
			console.log("ERROR: " + text);
			write(LOG, "ERROR", text);
		}
}

// logs error information: for ERROR, ALL, DEBUG modes
function warn(text) {
	if (global.config.loglevel == LEVELS.LOG_WARN ||
			global.config.loglevel == LEVELS.LOG_VERBOSE_ALL ||
			global.config.loglevel == LEVELS.LOG_VERBOSE_INCLUDE_DBG)
			write(LOG, "WARN", text);
}

// write debug information to log
function dbg(text) {
	if (global.config.loglevel == LEVELS.LOG_VERBOSE_INCLUDE_DBG) {
		write(LOG, "DBG", text);
		console.log("DBG: " + text);
	}
}

// add record to access log
function access(req) {
	try {
		var now = (new Date()).toUTCString();
		var method = req.method;
		var url = req.url;
		var ip = req.connection ? req.connection.remoteAddress : "?";
		var agent = req.headers ? req.headers['user-agent'] : "?";
		var s = url + " | " + agent;
		fs.appendFileSync(ACCESSLOG, '[' + now  + '][' + ip + '][' + method + '] ' + s + NL);
	} catch (e) {
		console.log(e);
	}
}

/** extend object WHAT with fields from WITHWHAT */
function extend(what, withwhat) {
	for (var x in withwhat) what[x] = withwhat[x];
	return what;
}

function isInteger(str) {
    var n = ~~Number(str);
    return String(n) === str && n >= 0;
}

/** remove sensitive information from error reports */
function strip(results, p) {
	var p1 = path.resolve(p);
	// remove file system facts
	for (res in results) {
		if (results[res].reason) {
			results[res].reason = results[res].reason.replace(p1, '...');
			results[res].reason = results[res].reason.replace(p, '...');
		}
		if (results[res].message) {
			results[res].message = results[res].message.replace(p1, '...');
			results[res].message = results[res].message.replace(p, '...');
		}
	}
	return results;
}

function cropText(string, len) {
	if (!len) len = 2048;
	if (!string) return "";
	if (string.length < len) return string;
	else return string.substring(0, len) + ' [...]';
}

function ensureTextGoodEnoughForDisplay(obj) {
	var displayMax = TEXT_LIMIT_K * 1024;
	if (typeof obj == "string") {
		return cropText(obj, displayMax);
	} else {
		if (typeof obj == "object")
			for (var k in obj) {
				obj[k] = ensureTextGoodEnoughForDisplay(obj[k]);
			}
	}
	return obj;
}

function normalizeEndlines(s) {
	return s
			.replace(/\r\n/g, '\n')
			.replace(/\n\r/g, '\n');
}


function mapfolder(folder) {
	log('Mapping ' + folder);
	var result = [];
	try {
		var filenames = fs.readdirSync(folder);
		filenames.forEach(
			(f) => {
				var full = path.join(folder, f);
				var text = fs.readFileSync(full, 'utf8');
				result[f] = text;
			}
		);
	} catch (e) {
		err("Error at heplers:mapfolder(): " + e.message);
	}
	return result;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function formatLocalDate(date) {
	var d = new Date(date);
	var pad = (x) => { return x > 9 ? x : '0' + x };
    return d.getFullYear()
        + '-' + pad(d.getMonth() + 1)
        + '-' + pad(d.getDate())
        + 'T' + pad(d.getHours())
        + ':' + pad(d.getMinutes())
        + ':' + pad(d.getSeconds());
}

function parseLocalDate(dateString) {
	var t = new Date(dateString.replace('T', ' '));
	return t;
}

module.exports = {
	monade,
	easymatch, lineseqmatch,
	sequence_size_match, set_size_match, seq_one_of_sizes_match,
	chmod777,
	log, dbg, err, warn,
	access,
	isInt : isInteger,
	extend,	strip, cropText, capitalizeFirstLetter,
	normalizeEndlines,
	mapfolder,
	formatLocalDate, parseLocalDate,
	ensureTextGoodEnoughForDisplay
}
