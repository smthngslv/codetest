var User = require('./domain.js').User;
const util = require('util');
var mailer = require('../model/email.js');
var session = require('./session.js');
var crypto = require('crypto');
var dal = require('./dal.js').UserData;
var h = require('./helpers.js');
global.config = global.config || require('../config.json');

session.init();

const afb = 'abcdefghijklmnopqrstuvwxyz0123456789';
/** generates password of given length using alphabet */
function generatePassword(len) {
    var r = '';
    for (var i = 0; i < len; i++)
	r += afb[parseInt(Math.random() * afb.length)];
    return r;
}

/** function that builds a hash using both username and pass */
function hpass(username, pwd) {
	var ha = crypto.createHash('sha256');
	ha.update(pwd + username + 'salt very salty salt');
	return ha.digest('hex');
}

function changepass(req, res, username, oldpass, newpass, callback) {
	authorize(req, res, username, oldpass,
		(sessionUser) => {			//success
			var userObj = dal.getUser(username);
			userObj.hash = hpass(username, newpass);
			dal.saveUser(userObj);
			//TODO: handle properly
			authorize(username, newpass, () => {}, () => {});
			callback('success');
		},
		(req, res, problem) => {	// fail
			callback('error', problem);
		}
	);
}

function cipher(txt, pwd) {
	const _cipher = crypto.createCipher('aes192', pwd);
	var encrypted = _cipher.update(txt, 'utf8', 'hex');
	encrypted += _cipher.final('hex');
	return encrypted;
}

function decipher(txt, pwd) {
	const _decipher = crypto.createDecipher('aes192', pwd);
	var decrypted = _decipher.update(txt, 'hex', 'utf8');
	decrypted += _decipher.final('utf8');
	return decrypted;
}

/** checks the user for either providing valid credentials, or being in session */
function authorize(req, res, uname, pass, callback, fallback) {
	h.access(req);
	var hasCred = typeof uname == "string";
	if (!hasCred) {
		callback = uname;
		fallback = pass;
	}
	if (hasCred) {
		login(uname, pass, (u, e) => {
			if (u) {
				h.log('[A] Successful login by password: ' + uname);
				// having credentials we generate new session key
				var key = "" + (new Date().getTime());
			    var s = session.setSession(req, res, key, uname);
			    s.userObject = u;
			    callback(s);
			} else {
			    h.err('[AUTH] Failed login by password: ' + uname);
			    session.killSession(req, res);
			    fallback(req, res, { message: e.message, code: 1 });
			}
		});
	} else {
		var sess = session.getSession(req);
		if (sess) {
		    callback(session.refreshSession(req, res));
		} else {
		    fallback(req, res, { message: "session not found/expired", code: 0 });
		}
	}
}

/** for pages with limited access - you should be euther root or owner */
function authStrict(req, res, uname, pass, callback, fallback) {
	h.access(req);
    var hasCred = typeof uname == "string";
    if (!hasCred) {
    	callback = uname;
		fallback = pass;
    }
    var cbproxy = function (cbp) {
    	var root = cbp.userObject.isRoot();
		var me = cbp.userObject.is(req.params.user_login);
	    if (root || me || !req.params.user_login) callback(cbp);
	    else fallback(req, res, { message: 'Not authorized', code: 2});
	};
    if (hasCred)
		authorize(req, res, uname, pass, cbproxy, fallback);
    else
		authorize(req, res, cbproxy, fallback);
}

/** for pages with limited access - you should be root */
function authRoot(req, res, uname, pass, callback, fallback) {
	h.access(req);
    var hasCred = typeof uname == "string";
    if (!hasCred) {
    	callback = uname;
		fallback = pass;
    }
    var cbproxy = function (cbp) {
    	var root = cbp.userObject.isRoot();
	    if (root) callback(cbp);
	    else fallback(req, res, { message: 'Not authorized', code: 2});
	};
    if (hasCred)
      authorize(req, res, uname, pass, cbproxy, fallback);
    else
  		authorize(req, res, cbproxy, fallback);
}

/** tries to match username+pass */
function login(username, pwd, callback) {
	var u = null;
	try { u = dal.getUser(username); }
	catch (e) { h.log('User ' + username + ' not found: ' + e.message); }
	if (u) {
		if (hpass(username, pwd) == u.hash) {
			callback(u, null);
		} else {
			callback(null, { message : 'wrong password' });
		}
	} else {
		callback(u, { message : 'user not found' });
	}
}

/** removes user from active sessions */
function logout(req, res) {
    session.killSession(req, res);
}

function send_pass(user, newpass, callback) {
  var NL = global.config.NEW_LINE;
	user.hash 	= hpass(user.login, newpass);
	var sign = "Yours, code.test()";

	var template = "Dear %s," + NL + NL +
		"New password was generated for you:" + NL +
		"Login: %s" + NL +
		"Password: %s" + NL + NL+
		"After login you will be able to " +
    "change your password in the Dashboard." + NL+ NL +"%s";
	var body = util.format(template, user.fullname, user.login, newpass, sign);
  var dao = { error_reason : null };

	mailer.send(user.email, "Password reset", body, false,
		(error, info) => {
			if (error) {
				dao.error_reason = "Could not send email: " + error;
			} else {
				user.persist();
				dao.success = true;
			}
			if (callback) callback(dao, newpass);
		});
}

function send_newpass(user, callback) {
  send_pass(user, generatePassword(6), callback);
}

module.exports = {
	authorize,
	authorizeStrict : authStrict,
	authorizeRoot : authRoot,
	login, logout,
	hpass, cipher, decipher,
	changepass, generatePassword,
  	send_newpass, send_pass
}
