const util = require('util');
const nodemailer = require('nodemailer');
const h = require('./helpers.js');
var auth = require('../model/auth.js');

function getConnstring() {
	var email = global.config.email;
	var pass = global.auth.decipher(email.epassword, global.config.pwd);
	var connstr = util.format("%s://%s:%s@%s",
			email.protocol,
			encodeURIComponent(email.login),
			encodeURIComponent(pass),
			email.server);
	return connstr;
}

function getTransporter() {
	if (!global.smtpsender) {
		var connstr = getConnstring();
		global.smtpsender =
			nodemailer.createTransport(connstr);
	}
	return global.smtpsender;
}

function send(to, title, body, isHTML, callback) {
	var mailOptions = {
		from: '"Code Test" <' + global.config.email.from + '>',
		to,
    subject: title
	};
	if (isHTML) {
		mailOptions.html = body;
	} else {
		mailOptions.text = body;
	}
	getTransporter().sendMail(mailOptions, function(error, info) {
	  if (error) h.err(error);
	  else h.log('Message sent: ' + info.response);
		if (callback) callback(error, info);
	});
}

module.exports = { send }
