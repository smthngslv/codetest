const path = require('path');
const proc = require('child_process');
const fs = require('fs');
const spawn = proc.spawn;
const config = require('../config.json');
var cmd = 'adduser';
const user = 'srexecutor';
var homedir = path.resolve(path.join('..', config.default_testrun_path));
console.log('Creating user ' + user + ' with homedir ' + homedir);  
var params = '--gecos "" --disabled-password --home ' + homedir + ' ' + user;
cmd = cmd + ' ' + params;
var pwd = process.argv[2];
proc.exec(cmd, { cwd: '.' }, (e, out, err) => {
	console.log('ERR1: ' + e);
	if (e) {
		console.log('ADDUSER STDOUT:');
		console.log(out);
		console.log('ADDUSER STDERR:');
		console.log(err);
	} else {
		// change password
		proc.exec('echo ' + user + ':' + pwd + ' | chpasswd', { cwd : '.' }, (_e, _out, _err) => {
			console.log('ERR2: ' + _e);
			if (_e) {
				console.log('CHPASSWD STDOUT:');
				console.log(_out);
				console.log('CHPASSWD STDERR:');
				console.log(_err);
			} else {
				proc.exec('id -u ' + user, {cwd : '.'}, (ee, out1, err1) => {
					console.log('ERR3: ' + ee);			
					if (!ee) {
						console.log('UID:  ' + out1);
						config.executor = parseInt(out1);
						fs.writeFileSync('../config.json', JSON.stringify(config, null, '\t'));
					} else {
						console.log('OOPS...');
					}
				});
			}
		});
	}
});
