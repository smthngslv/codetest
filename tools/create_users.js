var os = require('os');
var fs = require('fs');
var auth = require('../model/auth.js');
const afb = 'abcdefghijklmnopqrstuvwxyz';

function rndString(len) {
    var r = '';
    for (var i = 0; i < len; i++)
	r += afb[parseInt(Math.random() * afb.length)];
    return r;
}
function checkDirectorySync(directory) {
	try { fs.statSync(directory); }
	catch(e) { fs.mkdirSync(directory); }
}

if (process.argv.length < 3) {
	console.log('Please specify filename with user information:');
	console.log('');
	console.log('    node create_users.js <filename>');
	console.log('');
	console.log('File should contain raw user data with the following columns:');
	console.log('EMAIL, FULLNAME, STUDENT_ID, GROUP_ID, PASSWORD');
	console.log('');
	console.log('E.g.:');
	console.log('');
	console.log('j.doe@mit.org,John Alexander Doe,16M30001,BS2016-1,SomePassw0rD');
	console.log('ja.doe@mit.org,Jane Doe,16M30002,BS2016-2,OtherPas$word');
	console.log('');
} else {

	var source = process.argv[2];
	checkDirectorySync('groups');
	checkDirectorySync('users');
	var columns = ['email', 'fullname', 'student_id', 'group', 'pass'];
	var lines = fs.readFileSync(source, 'utf-8').trim().split('\n').map((x) => x.trim());

	for (var i in lines) {
	    var line = lines[i].split(',');
	    var u = {};
	    for (var j in columns) {
			u[columns[j]] = line[j];
			u.type = 'student';
	    }
	    var pass = u.pass || rndString(6);
	    u.hash = auth.hpass(u.email, pass);
	    var gf = u.group;
	    delete u.group;
	    delete u.pass;
	    console.log(u);
	    fs.appendFileSync('pass.txt', u.email + ',' + pass + ',' + u.hash + '\n');
	    fs.appendFileSync('groups/' + gf, u.email + '\n');
	    fs.writeFileSync('users/' + u.email, JSON.stringify(u, null, '\t'));
	}
}
