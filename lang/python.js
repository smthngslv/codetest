var ct = require('./compiler-tools.js');
var path = require('path');
var fs = require('fs-extra');

function compile(folder, callback) {
    var main = null;
    var PY_EXT = '.py';
    try {
        var py_files = fs
            .readdirSync(folder)
            .filter((f) => { return f.endsWith(PY_EXT); });
        if (py_files.length == 1) {
            main = py_files[0];
        } else {
            main = ct.search_substring(folder, PY_EXT, /if\W+__name__\W+==/);
        }
    } catch (e) {
    // if could not find - errormessage with code "NOMAIN"
        if (callback) callback({
            code : "NO_MAIN_METHOD",
            reason: e.message,
            method: e.method,
            stdout: e.message
        });
        return;
    }
    var full = path.join(folder, main);
    // pass w/o execution
    var exec_obj = {
        shell: 'python',
        params: [ full ],
        options: { cwd: folder }
    };
    var p = { code: 0, result: exec_obj };
    callback(p);
}

module.exports = {
  compile
}
