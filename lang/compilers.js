var fs = require('fs');
var path = require('path');

/**
* specific test area. You can add you reference here to checked at start-time
*/
var java = require('./java.js'), kotlin = require('./kotlin.js'),
  python = require('./python.js'), python3 = require('./python3.js'),
  cpp = require('./cpp.js'), cpp11 = require('./cpp11.js'), cs = require('./cs.js');

/**
* @ param {lang}  returns compiler object with general signature
*/
function get_compiler(lang) {
  var lf = path.join(__dirname, lang + '.js');
  if (fs.existsSync(lf)) {
    var cmp = require(lf);
    return cmp;
  } else {
    return null;
  }
}

function get_languages() {
  return require('../languages.json');
}

module.exports = {
  get_compiler,
  get_languages
}
