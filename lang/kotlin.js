var run = require('../model/run.js');
var h = require('../model/helpers.js');
var fs = require('fs');
const os = require('os');
var path = require('path');
var ct = require('./compiler-tools.js');


/**
* @ param {folder} folder to search for main class
*/
function find_kotlin_main(folder) {
	var files = fs.readdirSync(folder);
	//var evidence = ['fun main'];
	for (var i in files) {
		if (files[i].endsWith('.kt')) {
			let full = path.join(folder, files[i]);
			var content = String(fs.readFileSync(full));
			if (content.search(/fun\W+main/i) > -1) {
        		h.dbg('Kotlin main method found in: ' + full);
				return files[i];
			}
		}
	}

	throw { message: 'Kotlin main method not found in *.kt files',
		method : 'find_kotlin_main(folder)'};
}

function compile(folder, callback) {
	var compiler = 'kotlinc';
	var options = { shell: true }; //kotlinc is are script

	var kotlinmainclass;
	try {
    // class name
    kotlinmainclass = ct.search_substring(
        folder, '.kt', /fun\W+main/i)
        .replace('.kt', '');
	} catch (e) {
		if (callback)
      callback({
  			code : "-NOMAIN",
  			reason: e.message,
  			method: e.method,
  			stdout: e.message
  		});
		return;
	}

	var classname = h.capitalizeFirstLetter(kotlinmainclass) +'Kt';
	var params = ['*.kt'];
	var object_to_exec = { shell: 'kotlin', params: [ classname ], options: options };

	if (os.type() == 'Darwin' || os.type() == 'Linux') {
		fs.chmodSync(folder, '777')
	}
	run.execute(compiler, params, folder,
		global.config.compile.timeout * 1000,
		(p) => {
				p.result = p.code ? null : object_to_exec;
				if (callback) callback(p);
		}, options
	);
}

module.exports = {
  compile
}
