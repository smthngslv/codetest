var run = require('../model/run.js');
var h = require('../model/helpers.js');
var fs = require('fs');
var path = require('path');
var ct = require('./compiler-tools.js');

/**
* @ param {folder} folder with sources where files will be compiled
* @ param {callback} will accept either error object or data for execution step
*/
function compile(folder, callback) {
	// we will compile these guys
	var files = "*.cpp";
	// with this compiler
	var compiler = 'g++';

	// search for main class
	var mainfile;
	try {
		// class name
		mainfile = ct.search_substring(
			folder, '.cpp',
			/int\W+main/)
			.replace('.cpp', '');
	} catch (e) {
		// if could not find - errormessage with code "NOMAIN"
		if (callback) callback({
			code: "NO_MAIN_METHOD",
			reason: e.message,
			method: e.method,
			stdout: e.message
		});
		return;
	}

	var compile_params = [mainfile + '.cpp',
				'-std=c++14',
				'-o', 'a.exe'];
	var sh = path.join(folder, 'a.exe');
	var object_to_exec = { shell: sh, params: [], options: { cwd: folder, shell: true } };

	run.execute(compiler, compile_params, folder,
			global.config.compile.timeout * 1000,
			(p) => {
				// if compiled ok, we can execute then.
				// For execution we provide prepared shell and params
				p.result = p.code ? null : object_to_exec;
				if (callback) callback(p);
			}
	);
}

module.exports = { compile };
