var fs = require('fs');
var path = require('path');
var h = require('../model/helpers.js');


function search_substring(folder, ext, regex) {
  var files = fs.readdirSync(folder);
  for (var i in files) {
    if (files[i].endsWith(ext)) {
  		let full = path.join(folder, files[i]);
  		var content = String(fs.readFileSync(full));
  		if (content.search(regex) > -1) {
        h.dbg('Main method found in: ' + full);
  		return files[i];
  		}
  	}
	}
  throw { message: 'Main method not found in *' + ext + ' files',
    method : 'compiler-tools.search_substring(...)'};
}

module.exports = {
  search_substring
}
