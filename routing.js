var ui = {
	reports: require('./ui/reports.js'),
	auth   : require('./ui/auth.js'),
	contest: require('./ui/contest.js'),
	edit   : require('./ui/edit.js'),
	users  : require('./ui/users.js'),
	tools  : require('./ui/tools.js')
};
var Auth =  require('./model/auth.js');
var h =     require('./model/helpers.js');
var run =   require('./model/run.js');
var dom =   require('./model/domain.js'),
	User = dom.User,
	Problem = dom.Problem,
	Contest = dom.Contest,
	Attempt = dom.Attempt,
	Group = dom.Group;


/** what happens when you are not authorized or there's no such url
 */
function loginFallback(req, res, reason) {
	var isPost = req.method == "POST";
	h.dbg("LOGIN FALLBACK REQUESTURL = " + req.url + "; " + req.method);
	var returnurl = req.body.returnurl || req.params.returnurl || req.url;

	Auth.authorize(req, res, (session) => {
		if (returnurl && returnurl != "/") {
			res.redirect(returnurl);
		} else {
			res.redirect('/u/' + session.userObject.login + '/dashboard?reason=' + reason.message);
		}
	}, (m) => {
		res.redirect('/login?reason=' + reason.message + '&returnurl=' + returnurl);
	});
}

/**
* just stop the process
*/
function shutdown() {
	console.log("shutting down by request");
	process.exit(1);
}

/** statics and other dirty hacks
  */
function setup_static_and_predefined(app, express) {
    //TODO: automate routing for statics
    app.use('/c', express.static(__dirname + '/static'));

    app.get('/update-templates', (req, res) => {
    	h.access(req);
    	global.templates.update();
    	res.send('ok');
    });

		app.get('/shutdown', (req, res) => {
			  h.access(req);
				Auth.authorizeRoot(req, res, (session) => {
					shutdown();
				}, loginFallback);
		});
}

/** setup routing fot password management: change screens and reset screens
 */
function setup_passwd_management(app) {
    app.get('/changepass', (req, res) => {
    	Auth.authorize(req, res, (session) => {
    		ui.auth.chpass(req, res, session);
    	}, loginFallback);
    });

    app.post('/changepass', (req, res) => {
    	Auth.authorize(req, res, (session) => {
    		ui.auth.chpasspost(req, res, session);
    	}, loginFallback);
    });

    app.get('/resetpass', (req, res) => { ui.auth.rpass(req, res); });
    app.post('/resetpass', (req, res) => { ui.auth.rpasspost(req, res); });

    //TODO: close this later :)
    app.get('/gen/:user/:pass', (req, res) => {
    	h.access(req);
    	res.send(Auth.hpass(req.params.user, req.params.pass));
    });
}

/** setup routing for problem editing UI
 */
function setup_edit_problem(app) {
    // show problem editing UI
    app.get(Problem.editUrl(':user_login', ':problem_id'), (req, res) => {
    	Auth.authorizeRoot(req, res, (session) => {
    		ui.edit.problem(req, res, session);
    	}, loginFallback);
    });

    // upload problem
    app.post(Problem.editUrl(':user_login', ':problem_id'), (req, res) => {
    	Auth.authorizeRoot(req, res, (session) => {
    		ui.edit.problempost(req, res, session);
    	}, loginFallback);
    });
}

/** setup login and logout activities */
function setup_login_logout(app) {
    app.get('/login', (req, res) => {
    	h.access(req);
    	ui.auth.login(req, res);
    });

    app.post('/login', (req, res) => {
    	Auth.authorize(req, res, req.body.login, req.body.password,
    	(session) => {
			var url = req.body.returnurl;
			//TODO resolve login-redirect loop
			if (!url || url == "/" || url == "//")
				url = session.userObject.getUrlDashboard();
			h.log("POST redirect to " + url);
			res.redirect(url);
		}, loginFallback);
    });

    app.get('/logout', (req, res) => {
    	Auth.authorize(req, res, (session) => {
    	    Auth.logout(req, res);
    	    res.redirect('/login');
    	}, loginFallback);
    });
}

/** setup screens connected to problems in contest and attempts of problem
 */
function setup_contest_problems_screens(app) {
    // show problem for a user
    app.get(Problem.getUrl(':user_login', ':contest_id', ':problem_id'), (req, res) => {
    	Auth.authorizeStrict(req, res, (session) => {
    		ui.contest.problem(req, res, session);
    	}, loginFallback);
    });

    // trigger compilation
    app.post(Problem.getUrl(':user_login', ':contest_id', ':problem_id') + '/run', (req, res) => {
    	Auth.authorizeStrict(req, res, (session) => {
    		ui.contest.run(req, res, session);
    	}, loginFallback);
    });

		// trigger re-compilation
		app.post(Attempt.getUrl(':user_login', ':contest_id', ':problem_id', ':attempt') + '/rerun', (req, res) => {
			Auth.authorizeStrict(req, res, (session) => {
				ui.contest.rerun(req, res, session);
			}, loginFallback);
		});

		// trigger re-compilation for all last attempts
    app.post(Problem.getUrl(':user_login', ':contest_id', ':problem_id') + '/rerun-all', (req, res) => {
    	Auth.authorizeRoot(req, res, (session) => {
    		ui.contest.rerun_last(req, res, session);
    	}, loginFallback);
    });

    // show attempt
    app.get(Attempt.getUrl(':user_login', ':contest_id', ':problem_id', ':attempt'), (req, res) => {
    	Auth.authorizeStrict(req, res, (session) => {
    		ui.contest.attempt(req, res, session);
    	}, loginFallback);
    });

		// add attempt commenting
    app.post(Attempt.getCommentUrl(':user_login', ':contest_id', ':problem_id', ':attempt'), (req, res) => {
    	Auth.authorizeStrict(req, res, (session) => {
    		ui.contest.comment_attempt(req, res, session);
    	}, loginFallback);
    });

    app.post(Problem.getUrl(':user_login', ':contest_id', ':problem_id') + '/upload', (req, res) => {
    	Auth.authorizeStrict(req, res, (session) => {
    		ui.contest.upload(req, res, session);
    	}, (req, res) => { res.status(401).send('Unauthorized'); });
	});
}

/** setup all information panels, dashboards, reports
 */
function setup_dashboards_and_reports(app) {
    // redirect to user dashboard
    app.get('/', (req, res) => {
    	Auth.authorize(req, res, (session) => {
    		res.redirect('/u/' + session.user + '/dashboard');
    	}, loginFallback);
    });

    // render personal dashboard
    app.get(User.getUrlDashboard(':user_login'), (req,res) => {
    	Auth.authorizeStrict(req, res, (session) => { ui.reports.userDashboard(req, res, session); }, loginFallback);
    });

    // show contest for user
    app.get(Contest.getUrl(':user_login', ':contest_id'), (req, res) => {
    	Auth.authorizeStrict(req, res, (session) => {
    		ui.contest.overview(req, res, session);
    	}, loginFallback);
    });

    // list contest participants
    app.get(Contest.getUrl(':user_login', ':contest_id') +'/list', (req, res) => {
    	Auth.authorizeRoot(req, res, (session) => {
    		ui.contest.contest_standing(req, res, session);
    	}, loginFallback);
    });

	/* show group list to participant or admin */
    app.get(Group.getUrl(':user_login', ':group_id'), (req, res) => {
    	Auth.authorizeStrict(req, res, (session) => {
    	    ui.reports.group(req, res, session);
    	}, loginFallback);
    });

	/* show the list of all contests */
    app.get(Contest.allContestsUrl(), (req, res) => {
        Auth.authorizeRoot(req, res, (session) => {
    	    ui.reports.all_contests(req, res, session);
    	}, loginFallback);
    });
}

function setup_editors(app) {
	/* edit single contest */
	app.get(Contest.editUrl(), (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.contest.edit_contest(req, res, session);
		}, loginFallback);
	});

	app.delete(Contest.removeUrl(':contest_id'), (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.contest.delete_contest(req, res, session);
		}, loginFallback);
	});

	app.post(Contest.editUrl(), (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.contest.edit_contest_post(req, res, session);
		}, loginFallback);
	});

	app.get(Problem.editAllUrl(), (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.reports.all_problems(req, res, session);
		}, loginFallback);
	});

	app.delete(Problem.removeUrl(':problem_id'), (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.edit.delete_problem(req, res, session);
		}, loginFallback);
	});

	app.get(Group.editUrl(), (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.users.edit_groups(req, res, session);
		}, loginFallback);
	});

	app.post(Group.editUrl(), (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.users.edit_groups_post(req, res, session);
		}, loginFallback);
	});

	app.delete(Group.removeUrl(':gid'), (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.users.delete_group(req, res, session);
		}, loginFallback);
	});


	app.get(User.editUrl(), (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.users.edit_users(req, res, session);
		}, loginFallback);
	});

	app.post(User.editUrl(), (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.users.edit_user_post(req, res, session);
		}, loginFallback);
	});

	app.delete(User.removeUrl(':uid'), (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.users.delete_user(req, res, session);
		}, loginFallback);
	});

	app.get("/add-users-batch", (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.users.add_users_batch(req, res, session);
		}, loginFallback);
	});

	app.post("/add-users-batch", (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.users.add_users_batch_post(req, res, session);
		}, loginFallback);
	});
}

function setup_tools(app) {
	app.get("/sandbox", (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.tools.sandbox(req, res, session);
		}, loginFallback);
	});

	app.put("/sandbox-save", (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.tools.sandbox_save(req, res, session);
		}, loginFallback);
	});

	app.post("/sandbox-run", (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.tools.sandbox_run(req, res, session);
		}, loginFallback);
	});

	app.post(Contest.getJplagCreateReportUrl(':contest_id'), (req, res) => {
		Auth.authorizeRoot(req, res, (session) => {
			ui.contest.create_jplag_report(req, res, session);
		});
	});
}

module.exports = {
    setup_static_and_predefined,
    setup_passwd_management,
    setup_edit_problem,
    setup_login_logout,
    setup_contest_problems_screens,
    setup_dashboards_and_reports,
	setup_editors,
	setup_tools
}
