# README #

### What is this repository for? ###

* Web tool for automated code compiling and testing. Works for Windows and Linux. Runs on node.js.

### How do I get set up? ###

[Install node.js](https://nodejs.org/en/download/package-manager/).

Clone and install dependencies
```bash
git clone https://bitbucket.org/str-anger/stick-rope.git
cd stick-rope
npm install
cp config.json.example config.json
```

Update *config.json*.

* specify http port in **port** field.

* Set **pwd** field. This password will be used for symmetric encryption in system. 

* Update email setting to enable mail delivery (used for password recovery). To setup mail delivery tune **email** section. Set **email.epassword** value to aes192-encrypted password. You can do this with function
```javascript
node
> require('./model/auth.js').cipher('mailbox-password', 'pwd-field-from-previous-step-value')
> process.exit()
```
* For Linux system you need to create executing user with lower priorities (you can tune this user quotas later). This will create user and update *config.json* for you.
```bash
cd tools
sudo node create_system_executor_user.js
```
Other way: you can setup your own user which will execute compiled programs. Specify it's UID in **executor** field. To get UID type:
```bash
id -u <username>
```

### How to run server? ###

* Run server with
```bash
sudo node server.js
# OR 
sudo ./loop.sh
```

* Restart server with
```bash
sudo ./restart.sh
```

### How to manage languages? ###

* To setup new language compiler, please implement `lang\yourlang.js`. You implementation should export function `compile(folder, callback)`. Refer to example in `java.js`. Add your language to *languages.json* to appear in UI.

### How to manage users? ###

* You can add/edit/delete individual users in **Users** menu of any admin account. If you need to do bulk operation, run following command to see more details:
```bash
cd tools
node create_users.js
```

### JPlag reports ###

If you have [jPlag and Java](https://jplag.ipd.kit.edu/) installed on your server, you can generate plagiarism check reports with one button click at contest overview page. To do this, please tune your `config.json` file:
```
	"java": "/opt/jdk-13/bin/java",              # fresh java binary that works with jPlag
	"jplag_path": "./static/reports",            # path to store the reports. Suggested: static folder served by this app
	"jplag_jar": "/home/cvlab/jplag/jplag.jar",  # jPlag jar file
	"jplag_url": "/c/reports",                   # url prefix for the reports
```

### Other tools ###
Pay attention to the following scripts.

* `cleanup_executables.sh` removes all compiled files from test folder. Good idea before you backup the data.

* `backup.sh` creates a backup with a timestamp with all source, contest and solution files.

* `tools/search.sh some_string` searches for a some_string in all solution files. Naive way to detect copying.

### Whom to contact? ###

* http://sprotasov.ru/#tab:contacts
