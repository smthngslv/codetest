//TODO: remove. Left for compatibility
global.config = global.config || require('./config.json');
global.config.NEW_LINE = "\n";

var express = require('express');
var Auth = require('./model/auth.js');
var h = require('./model/helpers.js');
var swig = require('swig');
var path = require('path');
var routing = require('./routing.js');

var app = express();
function setup_express() {
	app.use(require('body-parser').urlencoded({ extended: false, limit: '50mb' }));
	app.use(require('cookie-parser')());
	app.on("error", function(error) {
		console.log(error);
		process.exit(123);
	});
        return app;
}

function setup_environment() {
	global.templates = {
		_templates: [],
		get : function (name) {
			if (!this._templates[name])
				this._templates[name] = swig.compileFile(path.join(__dirname, "/html/" + name + ".swig"));
			return this._templates[name];
		},
		update : function () {
			swig.invalidateCache();
			this._templates = [];
			this._templates.length = 0;
		}
	};
	global.auth = Auth;	
}

function setup_routing() {
	routing.setup_static_and_predefined(app, express);
	routing.setup_dashboards_and_reports(app);
	routing.setup_contest_problems_screens(app);
	routing.setup_edit_problem(app);
	routing.setup_passwd_management(app);
	routing.setup_login_logout(app);
	routing.setup_editors(app);
	routing.setup_tools(app);
	app.get('*', (req, res) => {
		Auth.authorize(req, res, (session) => {
			res.redirect(session.userObject.getUrlDashboard());
		}, (req, res, reason) => {
			res.redirect('/login?reason=' + reason.message);
		});
	});

}

// start single application, that will conflict with other domains
function start(srv, hostname, port) {
  try {
    var hn = hostname || "0.0.0.0";
    var p = port || 80;
    srv.listen(p, hn, () => {
        console.log('Codetest server ' + hn + ' is listening @:' + p);
    });
    h.log("HTTP server started: " + hn + ":" + p);
  } catch(e) {
    h.err(e);
    console.log('Error starting application: ' + e.message);
  }
}

module.exports = {
	setup_express, setup_environment, setup_routing, start
}
