var sessions = require('../model/session.js');
var dal = require('../model/dal.js').UserData;
var swig = require('swig');
var path = require('path');
var dom = require('../model/domain.js'),
	Contest = dom.Contest,
	Problem = dom.Problem,
	User = dom.User,
	Attempt = dom.Attempt,
	Group = dom.Group;
var h = require('../model/helpers.js');

function dataObject(session, ext) {
		var suser = dal.getUser(session.user);
		var result = { session_user: suser };
		if (ext) h.extend(result, ext);
		return result;
	}

function userDashboard(req, res, session) {
	var user = session.userObject;
	var d = dal.getUser(req.params.user_login);
	var groups = dal.getAllGroupsSync().filter((g) => { return g.users.indexOf(d.login) > -1 });
	var allContests = dal.getAllContestsSync();
	var current_contests = [], passed_contests = [], future_contests = [];
	for (var c in allContests) {
		var contest = allContests[c];
		var cgroups = contest.groups;
		var match = false;
		for (var g in groups) {
			if (cgroups.indexOf(groups[g].id) > -1) {
				match = true;
				break;
			}
		}
		var now = new Date();
		if (match) {
			if (contest.end < now) passed_contests.push(contest);
			else if (contest.start > now) future_contests.push(contest);
			else current_contests.push(contest);
		}
	}
	var stats = sessions.getSessionStatistics();
	stats.forEach((x) => {x.user = dal.getUser(x.login)});

	res.send(global.templates.get("dashboard")({
		session: session,
		user_name: session.user,
		session_user: user,
		display_user: d,
		groups: groups,
		sessions: stats,
		contests : {
			current: current_contests,
			passed: passed_contests,
			future: future_contests
		}
	}));
};

function group(req, res, session) {
	var users = dal.getGroupUsersByIdSync(req.params.group_id);
	var dao = dataObject(session, { group, users });
	res.send(global.templates.get('group')(dao));
}

function all_contests(req, res, session) {
	var dao = dataObject(session, {
		Problem,
		Contest,
		Group,
		contests: dal.getAllContestsSync()
	});
	res.send(global.templates.get('all-contests')(dao));
}

function all_problems(req, res, session) {
	var dao = dataObject(session, {
		Problem,
		problems: dal.getAllProblemsSync()
	});
	res.send(global.templates.get('all-problems')(dao));
}

module.exports = {
    userDashboard,
    group,
		all_contests,
		all_problems
};
