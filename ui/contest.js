var dal = require('../model/dal.js').UserData;
var swig = require('swig');
var path = require('path');
var multiparty = require('multiparty');
var fs = require('fs-extra');
var dom = require('../model/domain.js'),
	Contest = dom.Contest,
	Problem = dom.Problem,
	User = dom.User,
	Attempt = dom.Attempt,
	Group = dom.Group;
var Execution = require('../model/exec.js').Execution;
var runlib = require('../model/run.js');
var h = require('../model/helpers.js');
var compilers = require('../lang/compilers.js');

MAX_ACCEPTED_FILE_SIZE = 32 * 1024;

/** prepare initial data object fields for page render */
function dataObject(contest_or_id, duser, session, ext) {
	var id = contest_or_id;
	var suser = dal.getUser(session.user);
	// obtain or get contest
	var contest = typeof id === "string" ? dal.getContest(id) : id;
	var problems = dal.getAllProblemsByIdsSync(contest.problems);
	var result = {
		session_user: suser,
		display_user: duser,
		contest: contest,
		problems: problems
	};
	if (ext) h.extend(result, ext);
	return result;
}

/** contest screen rendering */
function overview(req, res, session) {
	var id = req.params.contest_id;
	var d = dal.getUser(req.params.user_login);
	var dao = dataObject(id, d, session);
	// contest not found - redirect
	if (dao == null) {
		res.redirect(session.userObject.getUrlDashboard());
		return;
	}
	dao.problems = dal.getAllProblemsByIdsSync(dao.contest.problems);
	dao.attempts = [];
	for (var p in dao.problems) {
		var problem = dao.problems[p];
		att = dao.contest.getLastUserAttempt(problem.id, d.login);
		if (att > 0)
			dao.attempts[problem.id] = dal.getAttempt(dao.contest, problem, d, att);
	}
	res.send(global.templates.get('contest-overview')(dao));
}

function rerun_last(req, res, session) {
	var problem = dal.getProblem(req.params.problem_id);
	var contest = dal.getContest(req.params.contest_id);
	var users = dal.getContestUsersSync(contest);

	Execution.rerunLastAttemptsForProblem(contest, problem, users, res, (results) => {
		//res.send(results);
	});
}

/** problem screen rendering */
function problem(req, res, session) {
	var pid = req.params.problem_id;
	var cid = req.params.contest_id;
	var d = dal.getUser(req.params.user_login);
	var dao = dataObject(cid, d, session);
	dao.problem = dal.getProblem(pid);
	dao.contest.passed = dao.contest.end < new Date() || dao.contest.start > new Date();

	dao.attempts = (dao.contest
		.getAttempts(dao.problem, d) || [])
		.sort()
		.reverse();

	var user = session.user;
	var pathToStore =
			path.join(global.config.default_code_path, cid, user, pid);
	h.log(pathToStore);
	//XXX: i don't like this solution, but uploading should be predecessed by cleaning
	runlib.clean(pathToStore, () => {
		res.send(global.templates.get('problem')(dao));
	});
}

//TODO: improve logging here
/** uploading files */
function upload(req, res, session) {
	var contest = req.params.contest_id;
	var user = session.user;
	var problem = req.params.problem_id;
	var pathToStore =
			path.join(global.config.default_code_path, contest, user, problem);

	var form = new multiparty.Form();
	var uploadFile = {uploadPath: '', type: '', size: 0};
	var errors = [];
	form.on('error', function(err) {
		h.dbg("Error during upload in contest:upload(req, res, session)");
		h.err(err);
		if(fs.existsSync(uploadFile.path)) {
			//if exists - delete
			//TODO: if we do cleaning before - do we need it?
			fs.unlinkSync(uploadFile.path);
		}
	});

	form.on('close', function() {
		if(!errors.length) { res.send(JSON.stringify({status: 'ok', text: 'Success'})); }
		else {
			if(fs.existsSync(uploadFile.path)) {
				fs.unlinkSync(uploadFile.path);
			}
			res.send(JSON.stringify({status: 'errors', errors: errors}));
		}
	});

	// when file comes
	form.on('part', function(part) {
		uploadFile.size = part.byteCount;
		uploadFile.type = part.headers['content-type'];
		uploadFile.path = path.join(pathToStore, part.filename);
		try {
			fs.mkdirsSync(pathToStore);
			h.chmod777(pathToStore);
		} catch (e) { if (e.code != 'EEXIST') throw e; }

		if (uploadFile.size > MAX_ACCEPTED_FILE_SIZE) {
			var errorMessage = 'File size is '
					+ parseInt(uploadFile.size / 1024) + 'KB. Limit is '
					+ (MAX_ACCEPTED_FILE_SIZE / 1024) + 'KB.';
			errors.push(errorMessage);
			res.status(400);
		}

		if (errors.length == 0) {
			var out = fs.createWriteStream(uploadFile.path);
			part.pipe(out);
		} else {
			part.resume();
		}
	});
	form.parse(req);
}

/** compile and run submitted code */
function run(req, res, session) {
	var dao = dataObject(req.params.contest_id, null, session);
	dao.problem = dal.getProblem(req.params.problem_id);
	var lang = req.body['lang'];
	var exec = new Execution(dao.session_user, dao.contest, dao.problem, lang);
	exec.compile_and_run_tests((x) => res.send(x));
}

/** compile and run submitted code */
function rerun(req, res, session) {
	var dao = dataObject(req.params.contest_id, null, session);
	var user = dal.getUser(req.params.user_login);
	var problem = dal.getProblem(req.params.problem_id);
	var attempt = dal.getAttempt(dao.contest, problem, user, req.params.attempt);
	var exec = new Execution(user, dao.contest, problem, req.query['lang']);
	exec.copy_code_from_attempt(attempt, () => {
		h.dbg("RERUN: code copied from attempt");
		exec.compile_and_run_tests((x) => res.send(x));
	});
}

/** show attempt files in tabs */
function attempt(req, res, session) {
	var d = dal.getUser(req.params.user_login);
	var dao = dataObject(req.params.contest_id, d, session);
	dao.problem = dal.getProblem(req.params.problem_id);
	dao.attempt = dal.getAttempt(dao.contest, dao.problem, d, req.params.attempt);
	//TODO: rework naming
	dao.Attempt = Attempt;
	res.send(global.templates.get('attempt')(dao));
}

function comment_attempt(req, res, session) {
	var bdy = req.body;
	var d = dal.getUser(req.params.user_login);
	var dao = dataObject(req.params.contest_id, d, session);
	dao.problem = dal.getProblem(req.params.problem_id);
	var att = dal.getAttempt(dao.contest, dao.problem, d, req.params.attempt);
	att.save_comments(bdy);
	res.send('ok');
}

/** shows contest participants with standing */
function contest_standing(req, res, session) {
	var isRaw = req.query.hasOwnProperty('raw');
	var d = dal.getUser(req.params.user_login);
	var dao = dataObject(req.params.contest_id, d, session);
	var groups = dao.contest.groups;
	dao.problems = dal.getAllProblemsByIdsSync(dao.contest.problems);
	// TODO rewrite using dal.getContestUsersSync
	var users = [];
	var ugroups = [];
	// collect all relevant student logins participating in contest
	for (var gr in groups) {
		var group = groups[gr];
		var g = dal.getGroup(group);
		for (var u = 0; u < g.users.length; u++) {
			var usr = g.users[u];
			if (!ugroups[usr]) {
				ugroups[usr] = ' ' + g.id + ' ';
			} else {
				if (ugroups[usr].indexOf(' ' + g.id + ' ') == -1)
					ugroups[usr] += g.id + ' ';
			}
			if (users.indexOf(usr) == -1)
				users.push(usr);
		}
	}

	// map user logins to objects
	var ulist = [];
	for (var i in users) {
		try {
			ulist.push(dal.getUser(users[i]));
		} catch (e) {
			h.warn("Error while reading users from groups. Non-existing user " +
				users[i] + " found");
		}
	}
	for (var x in ulist) ulist[x].groups = ugroups[ulist[x].login];
	dao.attempts = [];

	// prepare information for all users
	for (var u in ulist) {
		var login = ulist[u].login;
		dao.attempts[login] = { tests: [], passed: 0 };
		// we need info for all problems in contest
		for (var p in dao.problems) {
			var pid = dao.problems[p].id;
			// last results
			var att = dao.contest.getLastUserAttempt(pid, login);
			if (att > 0) {
				var attempt = dal.getAttempt(
								dao.contest,
								dao.problems[p],
								ulist[u], att);
				if (attempt) {
					if (attempt.result) {
						// number of passed tests for this problem
						var passed = attempt.result
								.filter((r) => {return r.code == 'OK';})
								.length;
						// number of passed tests at all
						dao.attempts[login].passed += passed;
						// codes in array and count of passed for the problem
						dao.attempts[login].tests[pid] = {
										codes	: attempt.result
													.map((r) => { return r.code; }),
										okcount	: passed,
										obj		: attempt,
										url		: attempt.getUrl(attempt)
								};
					}
				} else {
					dao.attempts[login].tests[pid] = {
						codes: [],
						okcount: 0
					}
				}
			}
		}
	}
	res.send(global.templates.get(isRaw ? 'contest-list-raw' : 'contest-list')(h.extend(dao, {users: ulist})));
}

function edit_contest(req, res, session) {
	var contest_id = req.query.cid;
	var contest = null;
	try {
		contest = new Contest(contest_id);
	} catch (e) {
		h.err("Contest:edit_contest(r,r,s): " + e.message);
		contest = new Contest();
		contest.id = contest_id;
	}

	var dao = dataObject(contest, null, session, { Contest });
	dao.contest = contest;
	dao.all_languages = compilers.get_languages();
	dao.problems = dal.getAllProblemNamesSync();
	dao.groups = dal.getAllGroupNamesSync();
	res.send(global.templates.get('contest-edit')(dao));
}

function edit_contest_post(req, res, session) {
		var contest_id = req.body.cid;
		var name = req.body.name;
		var problems = req.body.problems || [];
		if (typeof problems === 'string') problems = [ problems ];
		var groups = req.body.groups || [];
		if (typeof groups === 'string') groups = [ groups ];
		var langs = req.body.langs || [];
		if (typeof langs === 'string') langs = [ langs ];

		var start = req.body.start;
		var end = req.body.end;
		var is_strict = req.body.is_strict ? true : false;

		var contest = null;
		try { contest = new Contest(contest_id); }
		catch (e) {	var contest = new Contest(); contest.id = contest_id;	}

		contest.name = name;
		contest.problems = problems;
		contest.groups = groups;
		contest.langs = langs;
		contest.start = h.parseLocalDate(start);
		contest.end = h.parseLocalDate(end);
		contest.is_strict = is_strict;

		//save changes of new contest
		contest.persist();

		// and then reload the page
		var dao = dataObject(contest, null, session, { Contest });
		dao.contest = contest;
		dao.problems = dal.getAllProblemNamesSync();
		dao.groups = dal.getAllGroupNamesSync();
		dao.all_languages = compilers.get_languages();
		res.send(global.templates.get('contest-edit')(dao));
}

function delete_contest(req, res, session) {
		var cid = req.params.contest_id;
		h.dbg('Deleting contest ' + cid);
		try {
			new Contest(cid).suicide();
			res.status(200).send('ok');
		} catch (e) {
			h.dbg("Error in deleting contest contest:delete_contest(req, res, session)");
			h.err(e);
			res.status(500).send(JSON.stringify(e));
		}
}

function create_jplag_report(req, res, session) {
	var cid = req.params.contest_id;
	h.dbg("Running jplag report for " + cid);
	try {
		var ctst = new Contest(cid);
		ctst.runJplag((_) => { 
			res.redirect(dom.Contest.getUrl(session.user, cid));
		});
	} catch (e) {
		h.dbg("Error in running jPlag tools contest:create_jplag_report(req, res, session)");
		h.err(e);
		res.status(500).send(JSON.stringify(e));
	}
}

module.exports = {
	overview,
	problem,
	upload,
	attempt,
	run, rerun, rerun_last,
	contest_standing,
	edit_contest, edit_contest_post,
	delete_contest,
	comment_attempt,
	create_jplag_report
}
