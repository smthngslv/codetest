#!/bin/bash
while :; do
    if [ $(pgrep -f "node server.js" | wc -l) > 0 ]; then
        echo "Other server instances found. Killing..."
	pkill -f "node server.js"
    fi
    echo "Starting server..."
    node server.js
    ec=$?
    echo 'Server stopped with code ' $ec
    cdt="`date '+%Y-%m-%d %H:%M:%S'`";
    echo $cdt $ec >> loop.log
    sleep 2
    echo 'Server restarted at ' $cdt
done
